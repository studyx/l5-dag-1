## Opleiding Masterclass - Laravel Dag 1

### Lokale installatie via GIT

``git clone git@bitbucket.org:studyx/l5-dag-1.git``

### Homestead.yml aanpassen

    sites:
        - map: masterclass.dev
        to: /{yourlocation}/masterclass/public

    databases:
        - masterclass

### Homestead provision

``homestead provision``of indien de box al runt ``homestead up --provision``

### Database parameters correct plaatsen in .env

In .env de database paramaters aanpassen naar de correcte database.

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_DATABASE=masterclass
    DB_USERNAME=homestead
    DB_PASSWORD=secret

### Migraties uitvoeren

    php artisan migrate

### Seeds uitvoeren

    php artisan db:seed

### Ready



