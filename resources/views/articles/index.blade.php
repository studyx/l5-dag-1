@extends('app')

@section('titlepage') Artikelen overzicht @endsection

@section('content')
    <h1>Artikelen overzicht</h1>

    @if(count($articles) > 0)

        @foreach($articles as $article)
            <article>
                <h2>{{ $article->title }}</h2>
                <p>{{ $article->excerp }}</p>
                <a class="btn btn-primary" href="{{ action('ArticlesController@show', [$article->id]) }}">Lees meer</a>
            </article>
            <a class="btn btn-warning" href="{{ action('ArticlesController@edit', [$article->id]) }}">Wijzigen</a>

            {!!  Form::open(array('route' => array('articles.destroy', $article->id), 'method' => 'delete')) !!}
                <button class="btn btn-danger" type="submit" >Verwijderen</button>
            {!! Form::close() !!}

            <hr />
        @endforeach
    @else
        <p>Er zijn geen artikelen.</p>
    @endif

    <a class="btn btn-success" href="{{ action('ArticlesController@create') }}">Nieuw artikel</a>

@stop