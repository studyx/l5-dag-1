@extends('app')

@section('titlepage') Artikel - {{ $article->title }}} @endsection

@section('content')
    <h1>{{ $article->title }}</h1>
    <br>
    <div class="body">
        {{ $article->description }}
    </div>
    <br>
    <i>Aangemaakt op {{ $article->created_at }}</i>
    <br>
    <a class="btn btn-primary" href="{{ action('ArticlesController@index') }}">Terug naar overzicht</a>
@stop