@extends('app')

@section('titlepage') ARTIKEL AANMAKEN @endsection

@section('content')
    <h1>Maak een artikel aan</h1>

    {!! Form::open(['url' => 'articles']) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('excerp', 'Excerp') !!}
            {!! Form::textarea('excerp', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Verzenden', ['class' => 'bnt btn-primary form-control']) !!}
        </div>


    {!! Form::close() !!}

    @if($errors->any())
        @foreach($errors->all() as $error)
            {{ $error }}
        @endforeach
    @endif

@stop