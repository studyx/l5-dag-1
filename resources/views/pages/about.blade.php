@extends('app')

@section('titlepage') ABOUT PAGE @endsection

@section('content')
    <h1>About</h1>
    {{-- Foreach --}}
    @if(count($people) > 0)

        @foreach($people as $person)
            {{ $person }}
        @endforeach
    @else
        <p>Er zijn geen mensen</p>
    @endif

@stop
